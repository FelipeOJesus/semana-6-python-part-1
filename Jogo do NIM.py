def computador_escolhe_jogada(n, m):

    print("\n")

    if(m >= n):
        print("O computador tirou", n, "peça.")
        print("Fim do jogo! O computador ganhou!")
        return n;
    
    tirarPedra = n % (m+1)

    if(tirarPedra == 0):
        tirarPedra = m

    print("O computador tirou", tirarPedra, "peça.")

    if(tirarPedra > 0 and tirarPedra <= m):
        n = n - tirarPedra;
        vencedor(n)

        if(n == 1):
            print("Agora resta apenas uma peça no tabuleiro.")
        elif(n == 0):
            print("Fim do jogo! O computador ganhou!")
        else:
            print("Agora restam", n, "peças no tabuleiro.")

        
        return tirarPedra

    if(n == 1):
        print("Agora resta apenas uma peça no tabuleiro.")
    elif(n == 0):
        print("Fim do jogo! O computador ganhou!")
    else:
        print("O computador tirou", tirarPedra, "peça.")

    
    return tirarPedra

    

def usuario_escolhe_jogada(n, m):
    
    print("\n")
    tirarPedra = int(input("Quantas peças você vai tirar? "))
    
    while(tirarPedra > m or tirarPedra > n or tirarPedra <= 0):
        print("\n")
        print("Oops! Jogada inválida! Tente de novo.")

        print("\n")
        tirarPedra = int(input("Quantas peças você vai tirar? "))

    n = n - tirarPedra;

    vencedor(n)

    print("\n")
    
    print("Você tirou", tirarPedra, "peça.")
    
    if(n == 1):
        print("Agora resta apenas uma peça no tabuleiro.")
    elif(n == 0):
        print("Fim do jogo! Você ganhou!")
    else:
        print("Agora restam", n, "peças no tabuleiro.")

    
    return tirarPedra
    
def vencedor (n):
    if(n <= 0):
        return True
    return False
    
def partida(): 
    print("\n")  
    n = int(input("Quantas peças? "))
    m = int(input("Limite de peças por jogada? "))

    jogador = ""

    print("\n")

    if(n % (m+1) == 0):
        print("Voce começa!");
        jogador = "Voce"
    else:
        print ("Computador começa!");
        jogador = "computador"
        
    while(not vencedor(n)):
        if(jogador == "computador"):
            vencedor(n)
            n = n - computador_escolhe_jogada(n, m);
            if(vencedor(n)):
                break;
            else:
                n = n - usuario_escolhe_jogada(n, m)
                vencedor(n)
        else:
            n = n - usuario_escolhe_jogada(n, m)

            if(vencedor(n)):
                break;
            else:
                n = n - computador_escolhe_jogada(n, m);
                vencedor(n)
                
def campeonato():
    vitoriaDoUsuario = 0;
    vitoriaDoComputador = 0;

    while(vitoriaDoUsuario + vitoriaDoComputador < 3):
        print("\n")
        print("**** Rodada", (vitoriaDoUsuario + vitoriaDoComputador) + 1, "****")
        partida()
        vitoriaDoComputador += 1

    print("\n")
    print("**** Final do campeonato! ****")

    print("\n")
    print("Placar: Você", vitoriaDoUsuario, "X", vitoriaDoComputador, "Computador");

    print("\n")

def main():
    print("\n")
    print("Bem-vindo ao jogo do NIM! Escolha: ")
    print("\n")

    opcaoDeJogo = int(input("1 - para jogar uma partida isolada \n2 - para jogar um campeonato "))

    print("\n")

    if(opcaoDeJogo == 1):
        print ("Voce escolheu uma partida isolada!")
        partida()
    if(opcaoDeJogo == 2):
        print("Voce escolheu um campeonato!")
        campeonato()

main()

'''
EXEMPLO DADO PELO PROFESSOR 


Bem-vindo ao jogo do NIM! Escolha:

1 - para jogar uma partida isolada
2 - para jogar um campeonato 2

Voce escolheu um campeonato!

**** Rodada 1 ****

Quantas peças? 3
Limite de peças por jogada? 1

Computador começa!

O computador tirou uma peça.
Agora restam 2 peças no tabuleiro.

Quantas peças você vai tirar? 2

Oops! Jogada inválida! Tente de novo.

Quantas peças você vai tirar? 1

Você tirou uma peça.
Agora resta apenas uma peça no tabuleiro.

O computador tirou uma peça.
Fim do jogo! O computador ganhou!

**** Rodada 2 ****

Quantas peças? 3
Limite de peças por jogada? 2

Voce começa!

Quantas peças você vai tirar? 2 
Voce tirou 2 peças.
Agora resta apenas uma peça no tabuleiro.

O computador tirou uma peça.
Fim do jogo! O computador ganhou!

**** Rodada 3 ****

Quantas peças? 4
Limite de peças por jogada? 3

Voce começa!

Quantas peças você vai tirar? 2
Voce tirou 2 peças.
Agora restam 2 peças no tabuleiro.

O computador tirou 2 peças.
Fim do jogo! O computador ganhou!

**** Final do campeonato! ****

Placar: Você 0 X 3 Computador

'''
